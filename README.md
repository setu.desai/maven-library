# Maven library project sample

This project sample shows the usage of the Maven GitLab CI template for a library.

It demonstrates how to use the Maven GitLab CI template to deploy to [GitLab Maven Repository](https://docs.gitlab.com/ee/user/packages/maven_repository/).

## Maven template features

This project uses the following features from the Maven template:

* Overrides the default Maven docker image by declaring the `$MAVEN_IMAGE` variable in the `.gitlab-ci.yml` file,
* Enables snapshot & release by declaring the `$MAVEN_DEPLOY_ENABLED` variable in the `.gitlab-ci.yml` file +
  defines :lock: `$GIT_PRIVATE_KEY` secret (SSH private key, registered as a project [Deploy Key](https://docs.gitlab.com/ee/user/project/deploy_keys/)),
* Defines the `SONAR_HOST_URL` (enables SonarQube analysis),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `pom.xml`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable.

The Maven template also implements [unit tests report](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) and
[code coverage](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing) integration in GitLab.

This project uses the following features from the GitLab CI Maven template:

* Overrides the default Maven docker image by declaring the `$MAVEN_IMAGE` variable in the `.gitlab-ci.yml` file,
* Enables snapshot & release by declaring the `$MAVEN_DEPLOY_ENABLED` variable in the `.gitlab-ci.yml` file.

The GitLab CI Maven template also implements unit tests and code coverage integration in GitLab:

* unit tests results and code coverage are automatically integrated in merge requests,
* [code coverage badge](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-report-badge) (see above).

### Snapshot & Release implementation details

As stated in the [GitLab CI Maven template documentation](https://to-be-continuous.gitlab.io/doc/ref/maven/#mvn-snapshot-mvn-release-jobs),
enabling shapshots and releases have several requirements:

* declare `<distributionManagement>` section [in the project pom](./pom.xml#L21) that declares the snapshot and release repositories to use,
* declare the [Maven Repository credentials](./.m2/settings.xml#L10) in the `.m2/settings.xml` file,
    * if using GitLab Maven Repository: declared `Job-Token` header [as documented here](https://docs.gitlab.com/ee/user/packages/maven_repository/index.html#use-the-gitlab-endpoint-for-maven-packages)
    * if using an external repository: declare `<username>` and `<password>` (or any required credential), passing values with `${env.VARIABLE}` pattern to keep secrets out of Git.
* declare `<scm>` section [in the project pom](./pom.xml#L80) that declares the project `<developerConnection>` with provided `$CI_REPOSITORY_URL` (uses **https** protocol),
* as the `<developerConnection>` uses https protocol: declare `$GIT_USERNAME` and `$GIT_PASSWORD` (a GitLab private token) in the project variables (using **ssh** would have required to declare a private key or deploy key with `$GIT_PRIVATE_KEY`).

Snapshot versions are automatically published in the [GitLab Maven Repository](/to-be-continuous/samples/maven-library/-/packages/).

Releases can be performed manually from the pipeline, and are published in the [GitLab Maven Repository](https://gitlab.com/to-be-continuous/samples/maven-library/-/packages/).
